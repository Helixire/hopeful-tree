﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public DialogueTrigger[] soul;

    public void endGame()
    {
        FindObjectOfType<DialogueManager>().OnEndDialogue.AddListener(End);
        int failed = 0;
        foreach (DialogueTrigger d in soul)
        {
            if (d.helped())
            {
                if (failed == 0)
                {
                    d.fail();
                }
                ++failed;
            }
        }
    }

    public void End()
    {
        SceneManager.LoadScene("end");
    }
}
